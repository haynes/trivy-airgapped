FROM docker.io/aquasec/trivy:latest
LABEL maintainer="123haynes@gmail.com"

COPY db.tar.gz /
COPY javadb.tar.gz /
RUN mkdir -p ~/.cache/trivy/db && tar xvf /db.tar.gz -C ~/.cache/trivy/db && rm -rf /db.tar.gz
RUN mkdir -p ~/.cache/trivy/java-db && tar xvf /javadb.tar.gz -C ~/.cache/trivy/java-db && rm -rf /javadb.tar.gz

RUN trivy plugin install github.com/aquasecurity/trivy-plugin-webhook
COPY mattermost.tpl /contrib/mattermost.tpl
